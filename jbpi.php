<?php
require_once dirname( dirname( dirname( __DIR__ ) ) ). '/wp-config.php';
require_once __DIR__ . '/config.php';

if ( ( $cmd = \App\API\API::verifyRequest() ) !== false ) {
    if ( ( $output = \App\API\API::controller( $cmd ) ) !== false ) {
        echo $output;
    }
}
