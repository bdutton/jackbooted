<?php
namespace App\API;

class PingAPI extends BaseAPI {

    public static $commands = [
        'GET-/api/AlivePing' => [ 'cmd' => '\App\API\PingAPI->alivePing()', 'access' => API::INTERNAL ],
    ];

    public static function init () {
        parent::init();
    }

    public function __construct() {
        parent::__construct ();
    }

    public function alivePing() {
        $results = new APIResult();

        $results->set( 'answer', 'pong' );
        $results->set( 'time_sent', \Jackbooted\Time\Stopwatch::timeToDB() );

        return $results->JSON();
    }
}
