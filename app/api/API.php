<?php
namespace App\API;

use \Jackbooted\Forms\Request;

class API extends \Jackbooted\Html\WebPage {
    const DEF_INTERNAL_KEY = '5FKU8ziJoe20SzAVnOIyobeYIyWrydGS';
    const DEF_JS_KEY       = '8A78F813fAimNUQzzFwCufLsbBLsKuH1';
    const DEF_REMOTE_KEY   = 'xOnCbNGnXJjBpZHd6CCbzAJZ2WSovJQp';

    protected static $apiKeys = [
        self::DEF_INTERNAL_KEY             => [ 'access' => self::SYSTEM,     'user' => 'system',      'ips' => [ '0.0.0.0' ] ],
        self::DEF_JS_KEY                   => [ 'access' => self::JAVASCRIPT, 'user' => 'javascript',  'ips' => [ '0.0.0.0' ] ],
        self::DEF_REMOTE_KEY               => [ 'access' => self::CLIENT,     'user' => 'Scrape',      'ips' => [ '0.0.0.0' ] ],

        'X12jV4JJ3bgkbsB4VSI8AFG3sBzKdmdv' => [ 'access' => self::CLIENT,     'user' => 'unsset',      'ips' => [ '0.0.0.0' ] ],
        '4axc8BHc0kdHc5OFG8AgaTfegHrmD3sq' => [ 'access' => self::CLIENT,     'user' => 'unsset',      'ips' => [ '0.0.0.0' ] ],
        'gKOpi6SMJzmg1juc4kUwQQmt1qOL4V3Z' => [ 'access' => self::CLIENT,     'user' => 'unsset',      'ips' => [ '0.0.0.0' ] ],
    ];

    const API_KEY    = 'api_key';
    const API_KEY_1  = 'Api_key';

    const SYSTEM     = 0;
    const INTERNAL   = 5;
    const JAVASCRIPT = 8;
    const CLIENT     = 10;

    private static $commands = null;

    public static function init() {
        parent::init();
    }

    private static function initiate() {
        if ( self::$commands != null ) {
            return;
        }

        self::$commands = [];
        $cliDir = __DIR__;
        $classPrefix = "\\App\\API\\";
        $handle = opendir( $cliDir );
        while ( false !== ( $file = readdir( $handle ) ) ) {
            if ( strpos( $file, 'API.php' ) === false || in_array( $file, [ 'BaseAPI.php', 'API.php' ] ) ) {
                continue;
            }

            $fullClassName = $classPrefix . substr( $file, 0, -4 );

            self::$commands  = array_merge( self::$commands, $fullClassName::$commands );
        }
        closedir( $handle );
    }

    public static function doNotRun( $msg ) {
        $result = new APIResult();
        $result->addError( APIResult::SYSTEM_ERROR, $msg );
        echo $result->JSON();
        exit;
    }

    public static function verifyRequest() {
        if ( self::localRequest() ) {
            // This situation is where web server calls itself
            $apiUser = self::$apiKeys[self::DEF_INTERNAL_KEY];
        }
        else {
            if ( ( $apiKey = Request::get( self::API_KEY ) ) == '' ) {
                $headers = apache_request_headers();
                if ( isset( $headers[self::API_KEY] ) ) {
                    $apiKey = $headers[self::API_KEY];
                    Request::set( self::API_KEY, $apiKey );
                }
                else if ( isset( $headers[self::API_KEY_1] ) ) {
                    $apiKey = $headers[self::API_KEY_1];
                    Request::set( self::API_KEY, $apiKey );
                }
            }

            if ( $apiKey == '' ) {
                return self::doNotRun( self::API_KEY . ' is missing' );
            }
            if ( ! isset( self::$apiKeys[$apiKey] ) ) {
                return self::doNotRun( self::API_KEY . ' is invalid' );
            }

            $apiUser = self::$apiKeys[$apiKey];

            if ( ! self::compareIPToArray( $_SERVER['REMOTE_ADDR'], self::$apiKeys[$apiKey]['ips'] ) ) {
                return self::doNotRun( 'invalid IP. Your IP is not in whitelist' );
            }
        }

        self::initiate();
        $cmdID = $_SERVER['REQUEST_METHOD'] . '-' . $_SERVER['PATH_INFO'];
        if ( isset( self::$commands[$cmdID] ) ) {
            $cmdInfo = self::$commands[$cmdID];
            if ( $apiUser['access'] <= $cmdInfo['access'] ) {
                return $cmdInfo['cmd'];
            }
            else {
                return self::doNotRun( 'Access Denied' );
            }
        }
        else {
            return self::doNotRun( 'Unknown ' . $_SERVER['REQUEST_METHOD'] . ' Request: ' . $_SERVER['PATH_INFO'] );
        }
    }

    private static function localRequest() {
        return ( $_SERVER['SERVER_ADDR'] == $_SERVER['REMOTE_ADDR'] );
    }

    private static function compareIPToArray( $incomingIP, $acceptedIPList) {
        if ( in_array( '0.0.0.0', $acceptedIPList ) ) {
            return true;
        }

        if ( in_array( $incomingIP, $acceptedIPList ) ) {
            return true;
        }

        foreach ( $acceptedIPList as $ip ) {
            if ( substr( $ip, 0, 1) == '/' ) {
                if ( preg_match( $ip, $incomingIP ) == 1 ) {
                    return true;
                }
            }
        }

        return false;
    }
}
