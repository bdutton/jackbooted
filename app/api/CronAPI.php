<?php
namespace App\API;

class CronAPI extends BaseAPI {

    public static $commands = [
        'GET-/api/CronPing' => [ 'cmd' => '\App\API\CronAPI->cronPing()', 'access' => API::INTERNAL ],
    ];

    public static function init () {
        parent::init();
    }

    public function __construct() {
        parent::__construct ();
    }

    public function cronPing() {
        $results = new APIResult();

        $now = \Jackbooted\Forms\Request::get( 'fldNow', time() );
        $results->set( 'fldNow', $now );
        $results->set( 'fldNowTime', strtotime( $now ) );

        // Run the scheduler for the passed in time.
        $numberAdded = \Jackbooted\Cron\Scheduler::check( $now );
        $results->processed( $numberAdded, 'added_job_count' );

        $pageTimer = new \Jackbooted\Time\Stopwatch( 'Run time for ' . __METHOD__ );
        $cliResults = [];

        while ( $pageTimer->getTime() < 60 ) {
            $cronJobList = \Jackbooted\Cron\Cron::getList( 1 );
            if ( count( $cronJobList ) <= 0 ) {
                break;
            }

            foreach ( $cronJobList as $cronJob ) {
                $results->processed( 1, 'executed_job_count' );

                $startTime = \Jackbooted\Time\Stopwatch::timeToDB();

                $cronJob->status  = \Jackbooted\Cron\Cron::STATUS_RUNNING;
                $cronJob->runTime = $startTime;
                $cronJob->result  = 0;
                $cronJob->save();

                $jobLog = \App\Models\JobLog::factory( [ 'startTime' => $startTime, 'jobName' => $cronJob->command ] );

                if ( ( $commandComponents = preg_split( '/\s+/', $cronJob->command, -1, PREG_SPLIT_NO_EMPTY ) ) === false ) {
                    $msg = 'preg_split failed Invalid Command' . $cronJob->command;
                    $results->addError( APIResult::SYSTEM_ERROR, $msg );
                    $cronJob->status  = \Jackbooted\Cron\Cron::STATUS_ERROR;
                    $cronJob->message = $msg;
                    $cronJob->save();

                    $jobLog->endTime = \Jackbooted\Time\Stopwatch::timeToDB();
                    $jobLog->exitCode = -1;
                    $jobLog->jobOutput = $msg;
                    $jobLog->outputLen = strlen( $msg );
                    $jobLog->save();
                    continue;
                }

                $respJSON = ( new \App\Commands\CLI() )->index( $commandComponents );

                $cronJob->status  = \Jackbooted\Cron\Cron::STATUS_COMPLETE;
                $cronJob->save();

                $resp = json_decode( $respJSON, JSON_OBJECT_AS_ARRAY );
                $resp['cmd'] = $cronJob->getData();
                $cliResults[] = $resp;

                $jobLog->endTime = \Jackbooted\Time\Stopwatch::timeToDB();
                $jobLog->exitCode = 0;
                $jobLog->jobOutput = $respJSON;
                $jobLog->outputLen = strlen( $respJSON );
                $jobLog->save();
            }
            $results->set( 'job_results', $cliResults );
        }

        return $results->JSON();
    }
}
