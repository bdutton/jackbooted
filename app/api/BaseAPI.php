<?php
namespace App\API;

use \Jackbooted\Forms\Request;

class BaseAPI extends \Jackbooted\Html\WebPage {
    protected static $debug         = FALSE;
    protected static $debugMore     = FALSE;

    public static function init () {
        parent::init();

        date_default_timezone_set( 'UTC' );

        self::$debug     = ( Request::get( 'debug') == 'v' );
        self::$debugMore = ( Request::get( 'debug') == 'vv' );
        if ( self::$debugMore ) self::$debug = true;

        if ( self::$debug ) {
            \App\Registrar::debug();
        }
    }

    public static function JSON( $results ) {
        header( 'Content-type: application/json' );
        $jsonOpts = ( self::$debug ) ? JSON_PRETTY_PRINT : 0;
        return json_encode( $results, $jsonOpts ) . "\n";
    }

    public static function decodeBody() {
        $body = file_get_contents('php://input');
        if ( $body === false || $body == '' ) {
            return false;
        }

        if ( ( $decodedBody = json_decode( $body, true ) ) === null ) {
            return false;
        }

        return $decodedBody;
    }

    public static function checkMissingArgs( $args, $expected ) {
        $missing = [];
        foreach ( $expected as $arg ) {
            if ( ! isset( $args[$arg] ) ) {
                $missing[] = $arg;
            }
        }

        if ( count( $missing ) == 0 ) {
            return false;
        }

        return $missing;
    }
}